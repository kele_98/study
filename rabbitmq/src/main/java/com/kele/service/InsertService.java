package com.kele.service;

import org.springframework.amqp.core.Message;

/**
 * @author kele
 * @date 2021/8/13 11:15
 * @description
 */
public interface InsertService {

    /**
     * 发送需要新增的请求信息
     * @param message 新增的对象数据信息
     * @param exchangeName 交换机的名称
     * @param routingKey 路由key
     *
     */
    public boolean insertInfo(Message message, String exchangeName, String routingKey);
}
