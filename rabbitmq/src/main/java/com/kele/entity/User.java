package com.kele.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户类
 * @author kele
 */
@Data
public class User implements Serializable {

    private int id;
    private String name;
    private String sex;

    private String password;
}
