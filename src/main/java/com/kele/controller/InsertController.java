package com.kele.controller;

import com.kele.service.impl.InsertServiceImpl;
import com.kele.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author kele
 * @date 2021/8/13 11:26
 * @description
 */
@RestController
@RequestMapping("/insert")
public class InsertController {


    @Autowired
    private InsertServiceImpl insertService;

    @RequestMapping("/info")
    public R insertInfo(Object o){
        String exchangeName="direct_insert_exchange";
        String routingKey="user";
        insertService.insertInfo(o,exchangeName,routingKey);
        return R.ok();
    }
}
