package com.kele.controller;


import com.kele.entity.User;
import com.kele.service.impl.UserServiceImpl;
import com.kele.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author kele
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    UserServiceImpl userService;

    @GetMapping("/findAll")
    public R findAll(){

        List<User> userList = userService.findAll();

        if (userList.size()!=0){
            return R.ok(200,"查找全部用户信息成功").put("data",userList);
        }

        return R.error(404,"没有找到用户信息");

    }

    @GetMapping("/findById/{id}")
    public R findById(@PathVariable("id") Integer id){

        User user = userService.findUserById(id);

        if (user!=null){
            return R.ok(200,"通过ida查找用户信息").put("data",user);
        }

        return R.error(404,"没有找到用户信息");

    }
    @PutMapping("/update/{id}")
    public R updateUser(@PathVariable("id") Integer id,User user){

        User user1 = userService.findUserById(id);
        user1.setName("kele");
        user1.setSex("men");

        int i = userService.updateUser(user1);
        if (i>0){
            return R.ok(200,"修改成功");
        }
        return R.error(200,"修改失败");
    }

    @DeleteMapping("/delete/{id}")
    public R delete(@PathVariable("id") Integer id){

        int i = userService.deleteUser(id);
        if (i>0){
            return R.ok(200,"删除成功");
        }

        return R.error(200,"删除失败");
    }

    @PostMapping("/insert")
    public R insertUser(){
        User user1=new User();
        user1.setName("demo");
        user1.setSex("menAndwomen");

        int i = userService.insertUser(user1);
        if(i>0){
            return R.ok(200,"新增成功");

        }
        return R.error(200,"新增失败");
    }
}
