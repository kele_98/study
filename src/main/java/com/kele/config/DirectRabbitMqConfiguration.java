package com.kele.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author kele
 * @date 2021/8/13 11:08
 * @description
 */
@Configuration
public class DirectRabbitMqConfiguration {

    /**
     * 1.声明交换机的模式
     * 2.声明队列
     * 3.完成交换机和队列绑定
     *
     */

    @Bean
    public DirectExchange directExchange(){
        return new DirectExchange("direct_insert_exchange",true,false);
    }

    @Bean
    public Queue studentQueueDirect(){
        return new Queue("student.direct.queue",true);
    }

    @Bean
    public Queue userQueueDirect(){
        return new Queue("user.direct.queue",true);
    }


    @Bean
    public Binding studentBindingDirect(){
        return BindingBuilder.bind(studentQueueDirect()).to(directExchange()).with("student");
    }

    @Bean
    public Binding userBindingDirect(){
        return BindingBuilder.bind(userQueueDirect()).to(directExchange()).with("user");
    }

}
