package com.kele;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootAndCloudApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootAndCloudApplication.class, args);
    }

}
