package com.kele.service.impl;

import com.kele.service.InsertService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author kele
 * @date 2021/8/13 11:20
 * @description
 */
@Service
public class InsertServiceImpl implements InsertService {

    @Autowired
    private RabbitTemplate rabbitTemplate;


    @Override
    public void insertInfo(Object o, String exchangeName, String routingKey) {

        /**
         *通过mq实现分发
         */
        rabbitTemplate.convertAndSend(exchangeName,routingKey,o);

    }
}
