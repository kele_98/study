package com.kele.service.impl;


import com.kele.entity.User;
import com.kele.mapper.UserMapper;
import com.kele.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author kele
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    @Override
    public List<User> findAll() {
        return userMapper.findAll();
    }

    @Override
    public User findUserById(Integer id) {
        User user = userMapper.findUserById(id);
        return user;
    }

    @Override
    public User findUserByName(String name) {
        User user = userMapper.findUserByName(name);
        return user;
    }

    @Override
    public int updateUser(User user) {
        int i = userMapper.updateUser(user);

        return i;
    }

    @Override
    public int deleteUser(Integer id) {
        int i = userMapper.deleteUser(id);
        return i;
    }

    @Override
    public int insertUser(User user) {
        int i = userMapper.insertUser(user);
        return i;
    }


}
