package com.kele.service;

/**
 * @author kele
 * @date 2021/8/13 11:15
 * @description
 */
public interface InsertService {

    /**
     * 发送需要新增的请求信息
     * @param o 新增的对象数据信息
     * @param exchangeName 交换机的名称
     * @param routingKey 路由key
     *
     */
    public void insertInfo(Object o,String exchangeName,String routingKey);
}
