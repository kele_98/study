package com.kele.entity;

import lombok.Data;

/**
 * 用户类
 * @author kele
 */
@Data
public class User {

    private int id;
    private String name;
    private String sex;

    private String password;
}
