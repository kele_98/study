package com.kele.mapper;


import com.kele.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author kele
 */
@Mapper
public interface UserMapper {

    /**
     * 查找全部
     * @return
     */
    List<User> findAll();

    /**
     * 通过id查找用户
     * @return
     */
    User findUserById(@Param("id") Integer id);

    /**
     * 通过名字查用户
     * @param name
     * @return
     */
    User findUserByName(@Param("name") String name);

    /**
     * 修改用户
     * @param user
     * @return
     */
    int updateUser(User user);

    /**
     * 删除用户
     * @param id
     * @return
     */
    int deleteUser(Integer id);

    /**
     * 新增用户
     * @param user
     * @return
     */
    int insertUser(User user);

}
