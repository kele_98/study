package com.kele.mapper;

import com.kele.entity.Student;
import com.kele.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * @author kele
 * @date 2021/8/12 11:34
 * @description
 */
@Mapper
@CacheConfig(cacheNames = "student")
public interface StudentMapper {

    /**
     * 查找所有
     * @return
     */
    List<Student> findAll();


    /**
     * 通过id查找用户
     * @return
     * @param id
     */
    Student findStudentById(@Param("id") Integer id);

    /**
     * 通过名字查用户
     * @param name
     * @return
     */
    Student findStudentByName(@Param("name") String name);

    /**
     * 修改用户
     * @param student
     * @return
     */

    int updateStudent(Student student);

    /**
     * 删除用户
     * @param id
     * @return
     */
    int deleteStudent(Integer id);

    /**
     * 新增用户
     * @param student
     * @return
     */
    int insertStudent(Student student);

}
