package com.kele.service;

import com.kele.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author kele
 */
public interface UserService {

    /**
     * 查找全部
     * @return
     */
    List<User> findAll();

    /**
     * 通过id查找用户
     * @return
     */
    User findUserById(Integer id);

    /**
     * 通过名字查用户
     * @param name
     * @return
     */
    User findUserByName(@Param("name") String name);

    /**
     * 修改用户
     * @param user
     * @return
     */
    int updateUser(User user);

    /**
     * 删除用户
     * @param id
     * @return
     */
    int deleteUser(Integer id);

    /**
     * 新增用户
     * @param user
     * @return
     */
    int insertUser(User user);
}
