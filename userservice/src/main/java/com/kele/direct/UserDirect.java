package com.kele.direct;

import com.alibaba.fastjson.JSON;
import com.kele.entity.User;
import com.kele.service.impl.UserServiceImpl;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author kele
 * @date 2021/8/13 11:32
 * @description
 */

@Component
public class UserDirect {

    @Autowired
    UserServiceImpl userService;

    /**
     * 创建接受消息的方法
     *@RabbitHandler 表示消息的落脚点
     */

//    @RabbitListener(queues = {"user.direct.queue"})
//    public void receiveMsg(Message msg){
//
//        Object user = JSON.parse(msg.getBody());
//        String s = user.toString();
//
//        System.out.println("user direct ---接收到了要增加的信息是: "+ s);
//
//
//    }
}
