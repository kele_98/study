package com.kele.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author kele
 * @date 2021/8/12 11:30
 * @description
 */
@Data
public class Student implements Serializable {

    private Integer id;
    private String name;
    private String sex;
    private String birth;
}
