package com.kele.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Version 2019
 * @Author:kele
 * @Date:2021/8/7
 * @Content:
 * @Description：
 */
@Data
@AllArgsConstructor
public class User {

    private int id;
    private String name;
}
